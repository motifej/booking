(function() {
	'use strict';

	var patterns = {
		email: "\\w+.?\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,6}",
		currency: /^\d*\.{0,1}\d+$/
	};

	angular
		.module('booking')
		.value('patterns', patterns);

})();
