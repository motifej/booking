(function() {
  'use strict';

  angular
    .module('booking')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
