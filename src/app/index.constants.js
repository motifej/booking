(function() {
	'use strict';

	var suiteFee = 0.06;

	angular
		.module('booking')
		.constant('suiteFee', suiteFee);

})();
