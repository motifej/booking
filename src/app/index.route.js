(function() {
  'use strict';

  angular
    .module('booking')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('done', {
        url: '/',
        templateUrl: 'app/second/second.html',
        params: {
          travelers: null,
          total: null,
          fee: null 
        },
        controller: 'SecondController',
        controllerAs: 'second'
      });

  /*
    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });*/
    $urlRouterProvider.otherwise('/');
  }

})();
