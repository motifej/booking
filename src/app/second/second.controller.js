(function() {
  'use strict';

  angular
    .module('booking')
    .controller('SecondController', SecondController);


  function SecondController($stateParams, $state) {
    'ngIngect'

    var vm = this;

    vm.total = $stateParams.total;
    vm.travelers = $stateParams.travelers;
    vm.fee = $stateParams.fee;
    vm.goBack = goBack;

    function goBack() {
      $state.go('home');
    }

  }

})();
