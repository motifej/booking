(function() {
  'use strict';

  angular
    .module('booking')
    .controller('MainController', MainController);


  function MainController($scope, $log, $state, modalService, suiteFee, patterns) {
    'ngInject'

    var vm = this;

    vm.travelers = [];
    vm.traveler = {
      firstName: null,
      lastName: null,
      amount: null
    };
    vm.dt = new Date();
    vm.minDate = new Date();
    vm.formInvalid = false;
    vm.totalSumm=0;
    vm.paySumm=0;
    vm.testNumber = patterns.currency;
    vm.testEmail = patterns.email;
    vm.fee = 0;
    vm.email = null;
    vm.addTraveler = addTraveler;
    vm.amountChange = amountChange;
    vm.removeTraveler = removeTraveler;
    vm.sendForm = sendForm;
    vm.openCalendar = openCalendar;   

    initApp();

    function openCalendar($event) {
      $event.preventDefault();
      $event.stopPropagation();

      vm.opened = true;
    }

    function addTraveler() {
      vm.travelers.push(angular.copy(vm.traveler));      
    }

    function removeTraveler(id) {
      var traveler = vm.travelers.splice( id, 1 )[0];
      vm.totalSumm -= traveler.amount || 0;
    }

    function amountChange() {
      vm.totalSumm = vm.travelers.reduce( function(sum, item) {
        return sum += item.amount || 0;
      },vm.fee);
    }

    function initApp() {
      addTraveler();
      $scope.$watch( 
        function () {return vm.totalSumm},
        function () { vm.paySumm = (vm.totalSumm * ( suiteFee + 1 )) }
      );
    }

    function sendForm() {
      if ($scope.myForm.$invalid) {
        vm.formInvalid = true;
        return;
      }

      var modalOptions = {
          closeButtonText: 'Cancel',
          actionButtonText: 'Confirm',
          headerText: 'Booking',
          bodyText: 'Are you sure you want to buy it for ' + vm.paySumm.toFixed(2)+ '$ ?'
      };
      
      modalService.showModal({size: 'sm'}, modalOptions).then(function () {
        $state.go('done', {total: vm.paySumm, travelers: vm.travelers, fee: vm.fee});
      });
    }

  }

})();
