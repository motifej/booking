(function() {
	'use strict';

	angular
	.module('booking')
	.directive('validDate', validDate);

	function validDate() {

		var directive = {
			restrict: 'A',
			link: linkFunc,
			require: 'ngModel'
		};

		return directive;

		function linkFunc(scope, el, attr, ngModel) {
			var today = new Date().getTime() - 1000;
			ngModel.$validators.check = (function (value) {
				return value.getTime() > today;
			});

		}
	}    

})();