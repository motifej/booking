(function() {
  'use strict';

angular.module('booking').service('modalService', modalService);

function modalService ($modal) {
    'ngIngect'

    var modalDefaults = {
        backdrop: true,
        keyboard: true,
        modalFade: true,
        template: [
            '<div class="modal-header">',
                '<h3 class="modal-title">{{modalOptions.headerText}}</h3>',
            '</div>',
            '<div class="modal-body">',
                '<p>{{modalOptions.bodyText}}</p>',
            '</div>',
            '<div class="modal-footer modal-footer">',
                '<button class="btn btn-primary" ng-click="modalOptions.ok()">{{modalOptions.actionButtonText}}</button>',
                '<button class="btn btn-warning" ng-click="modalOptions.close()">{{modalOptions.closeButtonText}}</button>',
            '</div>'].join('')
    };

    var modalOptions = {
        closeButtonText: 'Close',
        actionButtonText: 'OK',
        headerText: 'Proceed?',
        bodyText: 'Perform this action?'
    };

    this.showModal = function (customModalDefaults, customModalOptions) {
        if (!customModalDefaults) customModalDefaults = {};
        customModalDefaults.backdrop = 'static';
        return this.show(customModalDefaults, customModalOptions);
    };

    this.show = function (customModalDefaults, customModalOptions) {
        //Create temp objects to work with since we're in a singleton service
        var tempModalDefaults = {};
        var tempModalOptions = {};

        //Map angular-ui modal custom defaults to modal defaults defined in service
        angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

        //Map modal.html $scope custom properties to defaults defined in service
        angular.extend(tempModalOptions, modalOptions, customModalOptions);

        tempModalDefaults.controller = function ($scope, $modalInstance) {
            'ngInject'

            $scope.modalOptions = tempModalOptions;
            $scope.modalOptions.ok = function () {
                $modalInstance.close();
            };
            $scope.modalOptions.close = function () {
                $modalInstance.dismiss('cancel');
            };
        }

        return $modal.open(tempModalDefaults).result;
    };

}

})();